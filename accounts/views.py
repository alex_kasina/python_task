from django.views.generic.base import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse

from .forms import UserCreationForm
from .utils import write_to_excel

import models as user_models

class UserListView(ListView):
    queryset = user_models.User.objects.order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        return context


class UserCreate(CreateView):
    model = user_models.User
    form_class = UserCreationForm


class UserDetails(DetailView):
    model = user_models.User

    def get_context_data(self, **kwargs):
        context = super(UserDetails, self).get_context_data(**kwargs)
        return context


class UserDelete(DeleteView):
    model = user_models.User
    success_url = reverse_lazy('user-list')


class UserEdit(UpdateView):
    model = user_models.User
    fields = ['username', 'birthday']
    template_name_suffix = '_update_form'

class ExportToExcel(View):
    queryset = user_models.User.objects.all()
    content_type = 'application/vnd.ms-excel'
    filename = 'users.xlsx'

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type=self.content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % self.filename
        xlsx_data = write_to_excel(self.queryset)
        response.write(xlsx_data)
        return response
