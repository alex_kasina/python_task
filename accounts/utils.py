from django.utils.translation import ugettext
import StringIO
import xlsxwriter
from accounts.templatetags.account_tags import eligible, bizzfuzz
 
def write_to_excel(qs):
    output = StringIO.StringIO()
    workbook = xlsxwriter.Workbook(output)
 
    worksheet_s = workbook.add_worksheet("Users data")
    
    title = workbook.add_format({
        'bold': True,
        'font_size': 14,
        'align': 'center',
        'valign': 'vcenter'
    })

    header = workbook.add_format({
        'bg_color': '#F7F7F7',
        'color': 'black',
        'align': 'center',
        'valign': 'top',
        'border': 1
    })

    # Add title
    title_text = u"Data for users"
    worksheet_s.merge_range('B2:E2', title_text, title)

    # add headers
    worksheet_s.write(4, 0, ugettext("Username"), header)
    worksheet_s.write(4, 1, ugettext("Birthday"), header)
    worksheet_s.write(4, 2, ugettext("Eligible"), header)
    worksheet_s.write(4, 3, ugettext("Random Number"), header)
    worksheet_s.write(4, 4, ugettext("BizzFuzz"), header)
    
    # Write data
    count = 0
    for obj in qs:
        row = 5 + count
        worksheet_s.write(row, 0, obj.username)
        worksheet_s.write(row, 1, obj.birthday.strftime('%d, %b %Y'))
        worksheet_s.write_string(row, 2, eligible(obj.birthday))
        worksheet_s.write_number(row, 3, obj.random_number)
        worksheet_s.write(row, 4, bizzfuzz(obj.random_number))

        count +=1
 
    workbook.close()
    xlsx_data = output.getvalue()
    # xlsx_data contains the Excel file
    return xlsx_data
