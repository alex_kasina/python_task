from django.conf.urls import url
import views as user_views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    
    url(r'^create/', user_views.UserCreate.as_view(), name='create-user'),
    url(r'^details/(?P<pk>[-\w]+)/', user_views.UserDetails.as_view(), name='user-details'),
    url(r'^delete/(?P<pk>[-\w]+)/', user_views.UserDelete.as_view(), name='delete-user'),
    url(r'^edit/(?P<pk>[-\w]+)/', user_views.UserEdit.as_view(), name='update-user'),
    url(r'^export/', user_views.ExportToExcel.as_view(), name='user-list-export'),
]
