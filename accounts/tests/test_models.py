from django.test import TestCase
from accounts.models import User
import datetime

class UserTest(TestCase):

    def create_user(self):
        return User.objects.create(username="testuser1", 
        birthday=datetime.date.today() - datetime.timedelta(days=12*365))

    def test_user_creation(self):
        user = self.create_user()
        self.assertTrue(isinstance(user, User))
        self.assertEqual(user.__unicode__(), user.username)
