from django.test import TestCase
from accounts.models import User
from django.core.urlresolvers import reverse
import datetime

class UserListTest(TestCase):

    def create_user(self, username):
        return User.objects.create(username=username, 
        birthday=datetime.date.today())

    def test_user_list(self):
        user1 = self.create_user(username="testuser1")
        user2 = self.create_user(username="testuser2")
        
        url = reverse("user-list")
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'accounts/user_list.html')
        self.assertIn('object_list', response.context)
        self.assertEqual(response.context['object_list'].count(), 2)


class UserCreateTest(TestCase):
    def test_user_create(self):
        
        url = reverse("create-user")
        response = self.client.post(url, {
            'username': "testuser1",
            'birthday':datetime.date.today(),
            'password1': 'testpass',
            'password2': 'testpass'
        })
        
        self.assertEqual(response.status_code, 302) # Redirect
        users = User.objects.all()
        self.assertEqual(users.count(), 1)


class UserDetailsTest(TestCase):
    def test_get_user_details(self):
        user = User.objects.create(username='testuser', 
        birthday=datetime.date.today())
        url = reverse("user-details", args=[user.pk])
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'accounts/user_detail.html')
        self.assertIn('object', response.context)
        self.assertEqual(response.context['object'], user)

class UserDeleteTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='testuser', 
                                        birthday=datetime.date.today())
        self.url = reverse("delete-user", args=[self.user.pk])
        
    def test_get(self):

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'accounts/user_confirm_delete.html')
        self.assertIn('object', response.context)
        self.assertEqual(response.context['object'], self.user)

    def test_post(self):
        users = User.objects.all()
        self.assertEqual(users.count(), 1)

        response = self.client.post(self.url, {
            'pk': self.user.pk
        })
        
        self.assertEqual(response.status_code, 302) # Redirect
        users = User.objects.all()
        self.assertEqual(users.count(), 0)

class UserEditTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='testuser', 
                                        birthday=datetime.date.today())
        self.url = reverse("update-user", args=[self.user.pk])


    def test_post(self):
        user1 = User.objects.get(id=self.user.id)
        self.assertEqual(user1.username, "testuser")

        response = self.client.post(self.url, {
            'username': "testuserupdated",
            'birthday':datetime.date.today(),
            'password1': 'testpass',
            'password2': 'testpass'
        })
        
        self.assertEqual(response.status_code, 302) # Redirect
        user = User.objects.get(id=self.user.id)
        self.assertEqual(user.username, "testuserupdated")
        self.assertNotEqual(user1.username, user.username)

class ExportToExcelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='testuser', 
                                        birthday=datetime.date.today())
        self.url = reverse("user-list-export")
    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEquals(response.get('Content-Disposition'),
                          "attachment; filename=users.xlsx")
