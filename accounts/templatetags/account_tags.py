from django import template
import datetime

register = template.Library()

@register.filter
def bizzfuzz(num):
    if num%3 == 0 and num%5 == 0:
        return "BizzFuzz"
    elif num%5 == 0:
        return "Fuzz"
    elif num%3 == 0:
        return "Bizz"
    else:
        return num

@register.filter
def eligible(dob):
    now = datetime.date.today()
    difference_in_years = ((now-dob).days)/365
    if difference_in_years > 13:
        return "allowed"
    else:
        return "blocked"
