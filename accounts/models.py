from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse_lazy
import random
import datetime


def random_num():
    return random.randint(1, 100)


class User(AbstractUser):
    birthday = models.DateField()
    random_number = models.IntegerField(default=random_num)

    def get_absolute_url(self):
        return reverse_lazy("user-details", kwargs={'pk': self.id})

    def __unicode__(self):
        return self.username
