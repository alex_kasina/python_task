How to run the project

===============================

1. install the dependencies

  `pip install -r requirements.txt`


 2. Run server

   `python manage.py runserver`

===============================

To run tests,

`python manage.py test`

===============================

Checklist for the deliverables

The task is made up of 5 parts plus an optional task. 

1. Set up a basic django 1.9 installation using a sqlite database in the same folder as the 
source

    COMPLETED


2. Add two fields to the User model using a migration: 
    1. a birthday field of type date 
       COMPLETED

    2. a random number field of type integer that is assigned a value from 1­ - 100 on 
creation
    COMPLETED


3. Create views for listing all users, viewing, adding, editing and deleting a single user
   COMPLETED

4. Create two template tags:
   1. A tag that will display "allowed" if the user is > 13 years old otherwise display 
"blocked"
    COMPLETED

    2. A tag that will display the BizzFuzz result of the random number that was 
generated for the user. The BizzFuzz specification is that for multiples of three 
print "Bizz" instead of the number and for the multiples of five print "Fuzz". For 
numbers which are multiples of both three and five print "BizzFuzz"
    COMPLETED

3. Add a column to the list view after the birthday column that uses the 
allowed/blocked tag
    COMPLETED

4. Add a column to the list view after the random number column that uses the 
BizzFuzz tag
    COMPLETED

5. Unit test what you feel is appropriate to test.

    COMPLETED (unittests for views and models)